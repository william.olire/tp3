package fr.uavignon.ceri.tp3.data;

import java.util.List;

public class WeatherResponse {
    public final Properties properties=null;

    public static class Properties {
        public final List<City> periods=null;
    }
}
