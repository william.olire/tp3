package fr.uavignon.ceri.tp3.data.webservice;


import fr.uavignon.ceri.tp3.data.WeatherResponse;

public interface OWMinterface {
    @Headers("Accept: application/geo+json")
    @GET("/gridpoints/{office}/{gridX},{gridY}/forecast")
    Call<WeatherResponse> getForecast(@Path("office") String office,
                                      @Path("gridX") int gridX,
                                      @Path("gridY") int gridY);

}
